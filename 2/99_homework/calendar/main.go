package main

import (
	"time"
	"math"
)

type Calendar struct {
	date time.Time
}

func (c *Calendar) CurrentQuarter() int {
	res := float64(c.date.Month()) / 3
	return int(math.Ceil(res))
}

func NewCalendar(time time.Time) Calendar {
	return Calendar{time}
}

