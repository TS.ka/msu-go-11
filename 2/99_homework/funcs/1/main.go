package main

import (
	"fmt"
	"bytes"
	"strings"
)

type memoizeFunction func(int, ...int) interface{}

var fibonacci memoizeFunction
var romanForDecimal memoizeFunction

func memoize(function memoizeFunction) memoizeFunction {
	cache := make(map[string]interface{})
	return func(n int, ns ...int) interface{} {
		key := fmt.Sprint(n)
		for _, i := range ns {
			key += fmt.Sprintf(",%d", i)
		}
		if _, exists := cache[key]; !exists {
			cache[key] = function(n, ns...)
		}
		return cache[key]
	}
}

func init() {
	fibonacci = memoize(func(n int, _ ...int) interface{} {
		if n <= 1 {
			return 1
		}

		return fibonacci(n - 1).(int) + fibonacci(n - 2).(int)
	})
	romanForDecimal = memoize(func(n int, ns ...int) interface{} {
		if n < 0 || n > 3999 {
			panic("romanForDecimal() only handles integers [0, 3999]")
		}

		decimals := []int{1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1}
		romans := []string{"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"}

		var buffer bytes.Buffer
		for i, decimal := range decimals {
			remainder := n / decimal
			n %= decimal
			if remainder > 0 {
				buffer.WriteString(strings.Repeat(romans[i], remainder))
			}
		}
		return buffer.String()
	})
}

func main() {
	fmt.Println("Fibonacci(45) =", fibonacci(45).(int))
	for _, x := range []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
		14, 15, 16, 17, 18, 19, 20, 25, 30, 40, 50, 60, 69, 70, 80,
		90, 99, 100, 200, 300, 400, 500, 600, 666, 700, 800, 900,
		1000, 1009, 1444, 1666, 1945, 1997, 1999, 2000, 2008, 2010,
		2012, 2500, 3000, 3999} {
		fmt.Printf("%4d = %s\n", x, romanForDecimal(x).(string))
	}
}
