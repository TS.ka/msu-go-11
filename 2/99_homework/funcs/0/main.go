package main

import (
	"fmt"
	"math"
)

// TODO: Реализовать вычисление Квадратного корня
func Sqrt(x float64) float64 {
	if (x < 0) {
		return 0.0
	} else if (x == 0) {
		return 0.0
	}
	var epsilon, delta, sqrt float64
	epsilon = 0.01
	delta = 0.001
	for sqrt = x; math.Abs(x - sqrt * sqrt) > epsilon; sqrt -= delta {}
	return sqrt
}

func main() {
	fmt.Println(Sqrt(2))
}
