package main

import (
	"strconv"
)

func ReturnInt() int {
	return 1
}

func ReturnFloat() float32 {
	return 1.1
}

func ReturnIntArray() [3]int {
	return [3]int{1, 3, 4}
}

func ReturnIntSlice() []int {
	return []int{1, 2, 3}
}

func IntSliceToString(slice []int) string {
	var result string
	for _, value := range slice {
		result += strconv.Itoa(value)
	}
	return result
}

func MergeSlices(slice1 []float32, slice2 []int32) []int {
	var merged []int
	for _, value := range slice1 {
		merged = append(merged, int(value))
	}
	for _,value := range slice2 {
		merged = append(merged, int(value))
	}
	return merged
}

func GetMapValuesSortedByKey(input map[int]string) []string {
	var result []string

	for len(input) > 0 {
		key := getMaxKeyFromMap(input)
		result = append(result, input[key])
		delete(input, key)
	}

	return flipSlice(result)
}

func getMaxKeyFromMap(input map[int]string) int {
	var max = 0
	for key := range input {
		if key > max {
			max = key
		}
	}
	return max
}

func flipSlice(input []string) []string {
	var result []string
	for i := len(input) - 1; i >= 0; i-- {
		result = append(result, input[i])
	}
	return result
}